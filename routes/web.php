<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');

Route::get('/master', function(){
    return view('layout.master');
});

Route::get('/table', function(){
    return view('tugasLaravel2.table');
});

Route::get('/data-table', function(){
    return view('tugasLaravel2.data-table');
});

// CRUD CAST
// Route::get('/cast/create', 'CastController@create');
// Route::post('/cast', 'CastController@store');
// Route::get('/cast', 'CastController@index');
// Route::get('/cast/{cast_id}', 'CastController@show');
// Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// Route::put('/cast/{cast_id}', 'CastController@update');
// Route::delete('/cast/{cast_id}', 'CastController@destroy');


// ORM CRUS CAST
Route::resource('cast', 'CastController');

// CRUD QUIZ 3 : Game
Route::get('/quiz3/create', 'GameController@create');
Route::get('/quiz3', 'GameController@index');
Route::post('/quiz3', 'GameController@store');
Route::get('/quiz3/{quiz3_id}', 'GameController@show');
Route::get('/quiz3/{quiz3_id}/edit','GameController@edit');
Route::put('/quiz3/{quiz3_id}','GameController@update');
Route::delete('/quiz3/{quiz3_id}','GameController@destroy');


Auth::routes();

