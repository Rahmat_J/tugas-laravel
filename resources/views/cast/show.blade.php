@extends('layout.master')

@section('title')
    Detail Pemain Film {{$cast->nama}}
@endsection

@section('content')
    <h3>{{$cast->nama}}</h3>
    <p>{{$cast->umur}}</p>
    <p>{{$cast->bio}}</p>
    <a href="/cast" class="btn btn-secondary my-2"> Kembali</a>
@endsection