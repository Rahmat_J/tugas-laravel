
@extends('layout.master')

@section('title')
    Dashboard
@endsection

@section('content')
    
<div>
    <h1>
            <b>Media Online</b>
        </h1>
        <p>
            <b>Sosial Media Developer</b>
        </p>
    </div>
    
    <div>
        <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>

        <div>
            <p>
                <b>Benefit Join di Media Online</b>
            </p>
            <ul>
                <li>Mendapatkan motivasi dari sesama para Developer</li>
                <li>Sharing knowlenge</li>
                <li>Dibuat oleh calon web developer terbaik</li>
            </ul>
        </div>

        <div>
            <p>
                <b>Cara Bergabung Ke Media Online</b>
            </p>
            <ol>
                <li>Mengunjungi Website ini</li>
                <li>Mendaftarkan di <a href="/register"> Form Sign Up</a></li>
                <li>Selesai</li>
            </ol>
        </div>
        
    </div>
@endsection