<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('tugasLaravel1.register');
    }

    public function welcome(Request $request){
        $firstName = $request['fname'];
        $lastName = $request['lname'];
        return view('tugasLaravel1.welcome', compact('firstName','lastName'));
    }
}
