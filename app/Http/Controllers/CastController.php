<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Cast;
use File;

class CastController extends Controller
{

    // MENGGUNAKAN QUERY BUILDER

    // public function create(){
    //     return view('cast.create');
    // }

    // public function store(Request $request){
        // $request->validate([
        //     'nama' => 'required',   
        //     'umur' => 'required',
        //     'bio' => 'required',
        // ]);

    //     DB::table('cast')->insert([
    //         'nama' => $request['nama'],
    //         'umur' => $request['umur'],
    //         'bio' => $request['bio']
    //     ]);

    //     return redirect('/cast');
    // }

    // public function index(){
    //     $cast = DB::table('cast')->get();

    //     return view('cast.index', compact('cast'));
    // }

    // public function show($id){
    //     $cast = DB::table('cast')->where('id', $id)->first();
    //     return view('cast.show', compact('cast'));
    // }

    // public function edit($id){
    //     $cast = DB::table('cast')->where('id', $id)->first();
    //     return view('cast.edit', compact('cast'));
    // }

    // public function update($id, Request $request){
    //     $request->validate([
    //         'nama' => 'required',   
    //         'umur' => 'required',
    //         'bio' => 'required',
    //     ]);

    //     $query = DB::table('cast')
    //         ->where('id', $id)
    //         ->update([
    //             'nama' => $request['nama'],
    //             'umur' => $request['umur'],
    //             'bio' => $request['bio'],
    //         ]);

    //     return redirect('/cast');
    // }

    // public function destroy($id){
    //     DB::table('cast')->where('id', $id)->delete();

    //     return redirect('/cast');
    // }

    

    // MENGGUNAKAN ELOQUENT MODEL

    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',   
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $cast = new Cast;
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;

        $cast->save();

        return redirect('/cast');
    }

    public function index(){
        $cast = Cast::all();

        return view('cast.index', compact('cast'));
    }

    public function show($id){
        $cast = Cast::findOrFail($id);


        return view('cast.show',compact('cast'));
    }

    public function edit($id){
        $cast = Cast::findOrFail($id);

        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',   
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $cast = Cast::find($id);

        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;

        $cast->update();
        return redirect('/cast');
    }

    public function destroy($id){
        $cast = Cast::find($id);
        $cast->delete();

        return redirect('/cast');
    }

}
