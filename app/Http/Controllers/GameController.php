<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GameController extends Controller
{
    public function create(){
        return view('quiz3.create');
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ]);

        DB::table('game')->insert([
            'name' => $request['name'],
            'gameplay' => $request['gameplay'],
            'developer' => $request['developer'],
            'year' => $request['year']
        ]);

        return redirect('/quiz3');
    }

    public function index(){
        $game = DB::table('game')->get();

        return view('quiz3.index', compact('game'));
    }

    public function show($id){
        $game = DB::table('game')->where('id', $id)->first();
        return view('quiz3.show', compact('game'));
    }

    public function edit($id){
        $game = DB::table('game')->where('id',$id)->first();
        return view('quiz3.edit', compact('game'));
    }

    public function update($id, Request $request){
        $request->validate([
            'name' => 'required',
            'gameplay' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ]);

        $game = DB::table('game')
        ->where('id',$id)
        ->update([
            'name' => $request['name'],
            'gameplay' => $request['gameplay'],
            'developer' => $request['developer'],
            'year' => $request['year']
        ]);

        return redirect('/quiz3');
    }

    public function destroy($id){
        DB::table('game')->where('id',$id)->delete();

        return redirect('/quiz3');
    }
}